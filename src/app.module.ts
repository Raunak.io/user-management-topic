import { UserModule } from './user/user.module';
import { Module } from '@nestjs/common';
import { AppController, ExperimentController } from './app.controller';
import { AppService,TaskService } from './app.service';
import config from './config/config';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { GoogleStrategy } from './auth/google.strategy';
import { MulterModule } from '@nestjs/platform-express';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    UserModule,
    ConfigModule.forRoot({ load: [config] }),
    MongooseModule.forRoot(process.env.DATABASE_URI),
    MulterModule.register({ dest: './uploads' }),
    ScheduleModule.forRoot()
  ],
  controllers: [AppController, ExperimentController],
  providers: [AppService, GoogleStrategy,TaskService],
})
export class AppModule {}
