import { Controller, Get,Post,Res,Param, Req, UseGuards, UseInterceptors, UploadedFiles } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthGuard} from '@nestjs/passport';
import {  FilesInterceptor} from '@nestjs/platform-express';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @UseGuards(AuthGuard('google'))

  async googleAuth(@Req() req){
 console.log(req);
  }

@Get('google/redirect')
@UseGuards(AuthGuard('google'))
googleAuthRedirect(@Req() req):any{

  return this.appService.googleLogin(req);
}

}

@Controller('newController')
export class ExperimentController {


@Post()
@UseInterceptors(FilesInterceptor('image'))

uploadFile(@UploadedFiles() file ){
console.log(file)
}

@Get(':imgpath')
seeUpldFile(@Param('imgpath') image,@Res() res){
  console.log(res,image)
return res.sendFile(image,{root:'uploads'});
}

}