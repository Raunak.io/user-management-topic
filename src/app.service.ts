import { Injectable,Logger } from '@nestjs/common';
import { Cron ,CronExpression, Interval, Timeout} from '@nestjs/schedule'; //,ScheduleRegistry

@Injectable()
export class AppService {

  googleLogin(req) {
    if (!req.user) {
      return 'there is no such user';
    }
console.log(req.user);
    return {
      message: 'user information recieved by google',
      user: req.user,
    };
  }
}


@Injectable()
export class TaskService{

//   constructor(private scheduleRegistry:ScheduleRegistry){}
// @Cron('2 * * * * *',{
//   name:'notification'
// })
// triggerNotification(){}

// const job = this.scheduleRegistry.getCronJob('notification');
// job.stop();
// console.log(job.lastDate());//string representation of last date of job executed


  private readonly logger = new Logger(TaskService.name);
  @Cron('45 * * * * *')
  // @Corn('* */30 9-17 * * *') every 30th min between 9 am to 5pm
  // @Corn('0 30 11 * * 1-5') mon to fri every 11:30 am
  // @Corn(CornExpression.EVERY_10_SECONDS) EVERY 10 SECOND
  handleCron(){
    this.logger.debug('automatically called at exact every 45th second')
  }


  @Interval(10000)
  handleInterval(){
    this.logger.debug('calling every 10 second')
  }
  
  @Timeout(5000)
  handleTimeout(){
    this.logger.debug('called once after 5 seconds')
  }
  
}