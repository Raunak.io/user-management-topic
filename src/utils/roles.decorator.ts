import { SetMetadata, CustomDecorator } from '@nestjs/common';

export const hasRoles = (...hasRoles: string[]): CustomDecorator<any> =>
  SetMetadata('roles', hasRoles);
