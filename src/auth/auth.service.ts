import { UserCreate } from './../schemas/user.schema';
import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { UserService } from './../user/user.service';
import {sign} from 'jsonwebtoken';
// import config from '../config/config';
import { Payload } from 'src/schemas/payload';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(  @Inject(forwardRef(() => UserService))  private userService: UserService,private configService: ConfigService) {}
  key = this.configService.get<string>('SECRET_KEY');
  
  async signPayload(payload: Payload):Promise<any> {
    return sign(payload, this.key, { expiresIn: '12h' });
  }

  async validateUser(payload: Payload):Promise<any>  {
    return await this.userService.findByPayload(payload);
  }


  
  sanitizeUser(user: UserCreate):any {
    const sanitized = user.toObject();
    // delete sanitized['password'];
    return sanitized;
 
  }

}