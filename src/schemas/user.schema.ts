import * as mongoose from 'mongoose';

import { IsString } from 'class-validator';
import validator from 'validator';

export const UserSchema = new mongoose.Schema({
  name: {
    required: [true, 'name field is required'],

    type: String,
  },
  email: {
    type: String,
    required: [true, 'email field is required'],
    unique: [true, 'email field is required'],
    lowercase: true,
    validate: [validator.isEmail, 'please enter your correct email address'],
  },

  role: {
    type: String,
    enum: ['admin', 'user'],
    default: 'user',
  },
  password: {
    required: [true, 'password is required'],
    minlength: [8, 'password should be minimum 8 characters'],

    type: String,
  },
});

export class UserCreate extends mongoose.Document {
  @IsString()
  readonly name?: string;
  @IsString()
  readonly email?: string;
  @IsString()
  readonly role?: string;
  @IsString()
  readonly password?: string;
 
}
export class UserLogin extends mongoose.Document {
  @IsString()
  readonly email: string;

  @IsString()
  readonly password: string;
}
