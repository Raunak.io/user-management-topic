import { ConfigService } from '@nestjs/config';
import { RolesGuard } from './../utils/role.guard';
import { JwtAuthGuard } from './../utils/jwt.guard';
import { JwtStrategy } from './../auth/jwt.strategy';
import { AuthService } from './../auth/auth.service';
import { UserSchema } from './../schemas/user.schema';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'User',
        useFactory: (): any => {
          const schema = UserSchema;

          schema.pre('save', () => console.log('add some logic'));
          return schema;
        },
      },
    ]),
  ],
  controllers: [UserController],
  providers: [UserService, AuthService, JwtStrategy,JwtAuthGuard,RolesGuard,ConfigService],
})
export class UserModule {}
