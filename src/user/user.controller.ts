import { hasRoles } from './../utils/roles.decorator';
import { JwtAuthGuard } from './../utils/jwt.guard';
import {  RolesGuard } from './../utils/role.guard';
import { AuthService } from './../auth/auth.service';
import { UserLogin, UserCreate } from './../schemas/user.schema';
import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Patch,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { UserService } from './user.service';
import { Payload } from '../schemas/payload';


@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
  ) {}

@hasRoles('admin')
@UseGuards( JwtAuthGuard,RolesGuard)
@Get()
  async getAllUsers(): Promise<any> {
    const data = await this.userService.getAllUsers();
    return { total: data.length, data };
  }

  @hasRoles('admin')
  @UseGuards( JwtAuthGuard,RolesGuard)
  @Get(':id')
  async getOneUser(@Param('id') userId: string): Promise<UserCreate> {
    const singleUser = await this.userService.getOneUser(userId);

    return singleUser;
  }

  @hasRoles('admin')
  @UseGuards( JwtAuthGuard,RolesGuard)
  @Post('create')
  async registerUser(@Body() userSignUpData: UserCreate): Promise<any> {
    const user = await this.userService.createUserD(userSignUpData);
    const payload: Payload = {
      name: user.name,
      email: user.email,
    };
    const token = await this.authService.signPayload(payload);
    console.log(token, user);
    return { user, token };
  }


  @hasRoles('admin')
  @Post('login')
  async loginUser(@Body() loginDat: UserLogin): Promise<any> {
    const user = await this.userService.login(loginDat);
    console.log(user);
    const payload: Payload = {
      name: user.name,
      email: user.email,
    };
    const token = await this.authService.signPayload(payload);
    console.log(token);
    return { user, token };
  }

  @hasRoles('admin')
  @UseGuards( JwtAuthGuard,RolesGuard)
  @Patch(':id')
  async updateUser(
    @Param('id') userId: string,
    @Body() updateData: UserCreate,
  ): Promise<string> {
    await this.userService.updateUser(userId, updateData);

    return 'updated';
  }

  @hasRoles('admin')
  @UseGuards( JwtAuthGuard,RolesGuard)
  @Delete(':id')
  async deleteUser(@Param('id') userId: string): Promise<string> {
    this.userService.deleteUser(userId);
    return 'user deleted successfully';
  }
}
