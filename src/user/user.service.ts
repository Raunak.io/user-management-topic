import { HttpStatus, Inject, forwardRef } from '@nestjs/common';
import { Payload } from './../schemas/payload';
import { UserLogin, UserCreate } from './../schemas/user.schema';
import { Injectable, NotFoundException, HttpException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AuthService } from './../auth/auth.service';
// import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @Inject(forwardRef(() => AuthService))
    private readonly authService: AuthService,
    @InjectModel('User') private readonly userModel: Model<UserCreate>,
  ) {}

  async login(userData: UserLogin): Promise<any> {
    const { email, password } = userData;
    console.log(userData);
    const user = await this.userModel.findOne({ email, password });
    // .select('+password');
    console.log(user);
    if (!user) {
      throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
    }
    return user;

    // if (await bcrypt.compare(password, user.password)) {
    //   console.log(user.password,password);
    //   return this.authService.sanitizeUser(user);
    // } else {
    //   throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
    // }
  }

  async createUserD(userSignDat: UserCreate): Promise<any> {
    const { email } = userSignDat;
    const user = await this.userModel.findOne({ email });
    if (user) {
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
    }

    const createdUser = new this.userModel(userSignDat);
    await createdUser.save();
    console.log(createdUser);
    return this.authService.sanitizeUser(createdUser);
  }

  async getOneUser(id: string): Promise<UserCreate> {
    const oneUser = await this.findUser(id);
    console.log(oneUser);
    return oneUser;
  }

  async getAllUsers(): Promise<UserCreate[]> {
    const res = await this.userModel.find();
    console.log(res);
    return res;
  }

  async updateUser(id: string, data: UserCreate): Promise<UserCreate> {
    console.log(data);
    await this.userModel.findByIdAndUpdate(id, data, {
      new: true,
      runValidators: false,
    });

    return;
  }

  async deleteUser(id: string): Promise<any> {
    await this.userModel.findByIdAndDelete(id);

    console.log(id);
    return;
  }

  private async findUser(id: string): Promise<UserCreate> {
    let user;
    try {
      user = await this.userModel.findById(id);
    } catch (error) {
      throw new NotFoundException('user not found . sorry!!');
    }
    if (!user) {
      throw new NotFoundException('user not found . sorry!!');
    }
    console.log(user);
    return user;
  }

  async findByPayload(payload: Payload): Promise<any> {
    const { email } = payload;
    return await this.userModel.findOne({ email });
  }
}
